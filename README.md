# README #
Personal vim configuration I use.

## Installation
First install Ack and platinum searcher.
```
brew install ack
brew install pt
```

This configuration uses the Hack font which includes Powerline symbols. Install it from the fonts submodule directory.
```
cd ~
git clone --recursive https://bitbucket.org/fucla/dotvim .vim
ln -s ~/.vim/.vimrc ~/.vimrc
```
