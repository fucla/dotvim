" base configuration {{{

  syntax enable
  filetype plugin indent on

  set timeoutlen=300              " mapping timeout
  set ttimeoutlen=50              " keycode timeout

  set mousehide                   " hide when characters are typed
  set history=1000                " number of command lines to remember
  set ttyfast                     " assume fast terminal connection

  set noshowmode                  " don't show default status line
  set encoding=utf-8              " set encoding for text
  set hidden                      " allow buffer switching without saving
  set autoread                    " auto reload if file saved externally
  set tabstop=2                   " show existing tab with 2 spaces width
  set shiftwidth=2                " when indenting with '>', use 2 spaces width
  set expandtab                   " on pressing tab, insert 2 spaces

  " orgmode {{{

    let maplocalleader="\<space>" " map localleader to space
    let org_indent=2              " body indentation

  "  }}}

" }}}

" functional {{{

  " ack {{{

    if executable('pt')
      let g:ackprg = 'pt --smart-case'
    endif

    " Search for word under cursor with the platinum searcher
    map <F2> :execute "Ack " . expand("<cword>") <CR>

  " }}}

  " Remove all trailing whitespace by pressing F3
  nnoremap <F3> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>

  " Clear inactive buffers
  function! Wipeout()
    "From tabpagebuflist() help, get a list of all buffers in all tabs
    let tablist = []
    for i in range(tabpagenr('$'))
      call extend(tablist, tabpagebuflist(i + 1))
    endfor

    "Below originally inspired by Hara Krishna Dara and Keith Roberts
    "http://tech.groups.yahoo.com/group/vim/message/56425
    let nWipeouts = 0
    for i in range(1, bufnr('$'))
      if bufexists(i) && !getbufvar(i,"&mod") && index(tablist, i) == -1
      " bufno exists AND isn't modified AND isn't in the list of buffers open in windows and tabs
         silent exec 'bwipeout' i
         let nWipeouts = nWipeouts + 1
      endif
    endfor
    echomsg nWipeouts . ' buffer(s) wiped out'
  endfunction
  command! Bdi :call Wipeout()

" }}}

" ui {{{

  if has('gui_running')
    set guioptions-=m        " remove menu bar
    set guioptions-=T        " remove toolbar
    set guioptions-=r        " remove right-hand scroll bar
    set guioptions-=L        " remove left-hand scroll bar
    set guifont=Hack:h14     " set font for gui
    set lines=50 columns=200 " set window size
    set background=dark      " set dark background
  endif

  set number                 " show line numbers
" }}}

" plugins {{{

  " set the runtime path to include Vundle and initialize
  set rtp+=~/.vim/bundle/Vundle.vim
  call vundle#begin()

  " Let Vundle manage Vundle, required
  Plugin 'VundleVim/Vundle.vim'
  Plugin 'itchyny/lightline.vim'
  Plugin 'scrooloose/nerdtree'
  Plugin 'kien/ctrlp.vim'
  Plugin 'tpope/vim-surround'
  Plugin 'jceb/vim-orgmode'
  Plugin 'tpope/vim-speeddating'
  Plugin 'mileszs/ack.vim'
  Plugin 'sonph/onehalf', {'rtp': 'vim/'}
  Plugin 'godlygeek/csapprox'

  " All of your Plugins must be added before the following line
  call vundle#end()            " required
  filetype plugin indent on    " required

  " Set colorscheme
  colorscheme onehalfdark

  " Set F1 hotkey for NERDTree
  map <F1> :NERDTreeToggle<CR>

  " Set up ctrl-p file searches
  let g:ctrlp_map = '<c-p>'
  let g:ctrlp_cmd = 'CtrlP'
  let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']

  " LightLine {{{
 
    let g:lightline = {
      \ 'colorscheme': 'onehalfdark',
      \ 'mode_map': { 'c': 'NORMAL' },
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ], [ 'fugitive', 'filename' ] ]
      \ },
      \ 'component_function': {
      \   'modified': 'LightlineModified',
      \   'readonly': 'LightlineReadonly',
      \   'fugitive': 'LightlineFugitive',
      \   'filename': 'LightlineFilename',
      \   'fileformat': 'LightlineFileformat',
      \   'filetype': 'LightlineFiletype',
      \   'fileencoding': 'LightlineFileencoding',
      \   'ctrlpmark': 'CtrlPMark',
      \   'mode': 'LightlineMode',
      \ }
    \ }

    function! LightlineModified()
      return &ft =~ 'help\|vimfiler\|gundo' ? '' : &modified ? '+' : &modifiable ? '' : '-'
    endfunction

    function! LightlineReadonly()
      return &ft !~? 'help\|vimfiler\|gundo' && &readonly ? '' : ''
    endfunction

    function! LightlineFilename()
      let fname = expand('%:t')
      return fname == 'ControlP' && has_key(g:lightline, 'ctrlp_item') ? g:lightline.ctrlp_item :
            \ ('' != LightlineReadonly() ? LightlineReadonly() . ' ' : '') .
            \ (&ft == 'vimfiler' ? vimfiler#get_status_string() :
            \  &ft == 'unite' ? unite#get_status_string() :
            \  &ft == 'vimshell' ? vimshell#get_status_string() :
            \ '' != expand('%:t') ? expand('%:t') : '[No Name]') .
            \ ('' != LightlineModified() ? ' ' . LightlineModified() : '')
    endfunction

    function! LightlineFugitive()
      if &ft !~? 'vimfiler\|gundo' && exists("*fugitive#head")
        let branch = fugitive#head()
        return branch !=# '' ? ' '.branch : ''
      endif
      return ''
    endfunction

    function! LightlineFileformat()
      return winwidth(0) > 70 ? &fileformat : ''
    endfunction

    function! LightlineFiletype()
      return winwidth(0) > 70 ? (&filetype !=# '' ? &filetype : 'no ft') : ''
    endfunction

    function! LightlineFileencoding()
      return winwidth(0) > 70 ? (&fenc !=# '' ? &fenc : &enc) : ''
    endfunction

    function! LightlineMode()
      let fname = expand('%:t')
      return fname == 'ControlP' ? 'CtrlP' :
           \ fname =~ 'NERD_tree' ? 'NERDTree' :
           \ winwidth(0) > 60 ? lightline#mode() : ''
    endfunction

    function! CtrlPMark()
      if expand('%:t') =~ 'ControlP' && has_key(g:lightline, 'ctrlp_item')
        call lightline#link('iR'[g:lightline.ctrlp_regex])
        return lightline#concatenate([g:lightline.ctrlp_prev, g:lightline.ctrlp_item
              \ , g:lightline.ctrlp_next], 0)
      else
        return ''
      endif
    endfunction

    let g:ctrlp_status_func = {
      \ 'main': 'CtrlPStatusFunc_1',
      \ 'prog': 'CtrlPStatusFunc_2',
      \ }

    function! CtrlPStatusFunc_1(focus, byfname, regex, prev, item, next, marked)
      let g:lightline.ctrlp_regex = a:regex
      let g:lightline.ctrlp_prev = a:prev
      let g:lightline.ctrlp_item = a:item
      let g:lightline.ctrlp_next = a:next
      return lightline#statusline(0)
    endfunction

    function! CtrlPStatusFunc_2(str)
      return lightline#statusline(0)
    endfunction

    " }}}

" }}}

" Set workspace directory
cd ~

